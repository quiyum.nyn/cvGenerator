
<!-- Javascript -->
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/jquery-1.11.2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/js/main.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/jsPDF/dist/jspdf.min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/jsPDF/libs/html2pdf.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/jsPDF/jspdf.js"></script>
<script type="application/javascript">
    $(document).ready(function () {



        $('.date_field').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });










        $("#date-popover").popover({html: true, trigger: "manual"});
        $("#date-popover").hide();
        $("#date-popover").click(function (e) {
            $(this).hide();
        });

        $("#my-calendar").zabuto_calendar({
            action: function () {
                return myDateFunction(this.id, false);
            },
            action_nav: function () {
                return myNavFunction(this.id);
            },
            ajax: {
                url: "show_data.php?action=1",
                modal: true
            },
            legend: [
                {type: "text", label: "Special event", badge: "00"},
                {type: "block", label: "Regular event", }
            ]
        });
    });


    function myNavFunction(id) {
        $("#date-popover").hide();
        var nav = $("#" + id).data("navigation");
        var to = $("#" + id).data("to");
        console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
</script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (1550);
        $('#message').fadeIn (1550);
        $('#message').fadeOut (50);

    })
</script>