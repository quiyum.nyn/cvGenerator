<script>
    $(document).ready(function() {
        var max_fields      = 1000; //maximum input boxes allowed
        var wrapper         = $("#input_fields_wrap3"); //Fields wrapper
        var add_button      = $("#add_field_button3"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><div class="col-md-11"><div class="form-group"><input type="text" class="form-control" name="literacy[]" placeholder="Computer Literacy" required /></div></div><a href="#" class="remove_field3 btn btn-danger col-md-1 col-lg-1"><i class="fa fa-times" aria-hidden="true"></i></a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field3", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>

