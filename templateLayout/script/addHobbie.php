<script>
    $(document).ready(function() {
        var max_fields      = 1000; //maximum input boxes allowed
        var wrapper         = $("#input_fields_wrap5"); //Fields wrapper
        var add_button      = $("#add_field_button5"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><div class="col-md-11"><div class="form-group"><input type="text" name="hobbie[]" class="form-control" placeholder="Hobby" required /></div></div><a href="#" class="remove_field5 btn btn-danger col-md-1 col-lg-1"><i class="fa fa-times" aria-hidden="true"></i></a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field5", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>

