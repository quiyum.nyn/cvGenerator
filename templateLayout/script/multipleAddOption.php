<script>
    $(document).ready(function() {
        var max_fields      = 1000; //maximum input boxes allowed
        var wrapper         = $("#input_fields_wrap"); //Fields wrapper
        var add_button      = $("#add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><br><div class="row"><div class="col-md-10 col-sm-10 col-xs-10"><div class="form-group"><input type="text" class="form-control" name="poll_option[]" placeholder="নতুন অপশন যোগ করুন"></div></div><a href="#" class="remove_field btn btn-danger col-md-2 col-sm-2 col-xs-2" style="width: 14%"><i class="fa fa-times" aria-hidden="true"></i></a></div></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>

<script type="text/javascript">
    function valid() {
        var city=document.getElementById("city").value;
        if(city=="selectProduct")
        {
            alert("All fields need to fill up!");
            return false;
        }
    }
</script>
