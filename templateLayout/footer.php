<!-- ******FOOTER****** -->
<footer class="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="footer-col col-md-3 col-sm-4 about">
                    <div class="footer-col-inner">
                        <h3>About</h3>
                        <ul>
                            <li><a href="<?php echo base_url;?>index.php"><i class="fa fa-caret-right"></i>Home</a></li>
                            <li><a href="<?php echo base_url;?>profile.php"><i class="fa fa-caret-right"></i>Profile</a></li>
                            <li><a href="<?php echo base_url;?>generate_cv.php"><i class="fa fa-caret-right"></i>Generate CV</a></li>
                        </ul>
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col-->
                <div class="footer-col col-md-6 col-sm-8 newsletter">
                    <div class="footer-col-inner">
                        <h3 style="text-align: center">Developer</h3>
                        <h4 style="text-align: center">Imran Hossain</h4>
                        <h5 style="text-align: center">Computer Science & Engineering</h5>
                        <h5 style="text-align: center">PCIU</h5>

                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col-->
                <div class="footer-col col-md-3 col-sm-12 contact">
                    <div class="footer-col-inner">
                        <h3>Contact us</h3>
                        <div class="row">
                            <p class="adr clearfix col-md-12 col-sm-4">
                                <i class="fa fa-map-marker pull-left"></i>        
                                <span class="adr-group pull-left">       
                                    <span class="street-address">Online CV Generator</span><br>
                                    <span class="postal-code">Imran Hossain</span><br>
                                    <span class="country-name">Port City International university</span>
                                    <span class="country-name">Chittagong</span>
                                </span>
                            </p>
                            <p class="tel col-md-12 col-sm-4"><i class="fa fa-phone"></i>01829320861</p>
                            <p class="email col-md-12 col-sm-4"><i class="fa fa-envelope"></i><a href="#">emonzhossainz@gmail.com</a></p>
                        </div>
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col-->
            </div>
        </div>
    </div><!--//footer-content-->
    <div class="bottom-bar">
        <div class="container">
            <div class="row">
                <small class="copyright col-md-8 col-sm-12 col-xs-12">Copyright @ 2017 Online CV Generator | Developed by: <a href="#" target="_blank">Emon Hossain</a></small>
                <ul class="social pull-right col-md-4 col-sm-12 col-xs-12">
                    <li><a href="#" ><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" ><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" ><i class="fa fa-youtube"></i></a></li>

                    <li><a href="#" ><i class="fa fa-google-plus"></i></a></li>

                    <li><a href="#" ><i class="fa fa-skype"></i></a></li>
                    <li class="row-end"><a href="#" ><i class="fa fa-rss"></i></a></li>
                </ul><!--//social-->
            </div><!--//row-->
        </div><!--//container-->
    </div><!--//bottom-bar-->
</footer><!--//footer-->