<?php
session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
if($_SESSION['complete']<100){
    Utility::redirect('profile.php');
}
use App\User_info;
$object=new User_info();
$object->prepareData($_SESSION);
$exist_userInfo=$object->isInserted();
if($exist_userInfo){
    $userInfo=$object->showUserInfo();
}
use App\Education_qualification;
$objEdu=new Education_qualification();
$objEdu->prepareData($_SESSION);
$exist_academic=$objEdu->isInserted();
if($exist_academic){
    $academic=$objEdu->showAcademic();
}
use App\Computer_literacy;
$objLiteracy=new Computer_literacy();
$objLiteracy->prepareData($_SESSION);
$exist_literacy=$objLiteracy->isInserted();
if($exist_literacy){
    $oneLiteracy=$objLiteracy->showLiteracy();
}


use App\Hobbies;
$objHobbiy=new Hobbies();
$objHobbiy->prepareData($_SESSION);
$exist_hobbie=$objHobbiy->isInserted();
if($exist_hobbie){
    $oneHobbie=$objHobbiy->showHobbie();
}


use App\Language_communication;
$languageObj=new Language_communication();
$languageObj->prepareData($_SESSION);
$existLanguage=$languageObj->isInserted();

if($existLanguage){
    if(isset($_GET['language_id'])){
        $languageObj->prepareData($_GET);
        $oneLanguage=$languageObj->showOneData();
    }
    else{
        $allLanguage=$languageObj->showLanguage();
    }

}
use App\Category;
$catObj=new Category();
$catObj->prepareData($_GET);
$oneCategoryData=$catObj->oneData();

use App\New_field;
$fieldObj=new New_field();
$fieldObj->prepareData($_SESSION);
$exist_field=$fieldObj->isInserted();

if($exist_field){
    $allNewField=$fieldObj->showNew();
}
if(isset($_POST['edit_objective']) && !empty($_POST['edit_objective'])) {
    $_SESSION['edit_objective'] = $_POST['edit_objective'];
    $_SESSION['edit_proclamation'] = $_POST['edit_proclamation'];
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
    <style>
        p{
            color: #fff;
        }

    </style>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div style="width: 100%;overflow: hidden">
        <div class="content container" style="width: 70%; float: left;overflow: hidden">
            <!--<button id="create_pdf">Create Pdf</button>-->
            <div style="width: 100%; height160%;font-family: 'Century Gothic';overflow: hidden;">
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Edit Objective & Proclamation</button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Edit Info</h4>
                            </div>
                            <div class="modal-body">
                                <form action="template5.php?category_id=<?php echo $_GET['category_id']?>" method="post">
                                    <label>Objective</label>
                                    <textarea rows="5" class="form-control" name="edit_objective"><?php echo $oneCategoryData->objective?></textarea>
                                    <br>
                                    <label>Proclamation</label>
                                    <textarea rows="5" class="form-control" name="edit_proclamation"><?php echo $oneCategoryData->proclamation?></textarea>
                                    <br>
                                    <input type="submit" class="btn btn-primary" value="Edit">
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                <button onclick="ExportPdf()" class="btn btn-primary">Download as PDF</button>
                <div style="width:100%;min-height: 1176px;overflow: hidden;border: 1px black solid" >
                    <div id="myCanvas" style="color: #fff;">
                        <div style="background-color: #392856;">
                            <div class="row">
                                <div style="width: 100%;height: 30px;background-color: #2178a9;">
                                </div>
                                    <div class="col-md-8" style="height: 160px;">
                                        <h4 style="text-align: center;line-height: 127px"><?php echo $userInfo->full_name?></h4>
                                    </div>
                                    <div class="col-md-2" style="height: 160px;">
                                        <img src="resources/user_photos/<?php echo $userInfo->picture?>" class="img-circle" width="100%" style="margin-top: 10px;border-radius: 5px">
                                    </div>
                            </div>
                            <div style="width: 80%;height: 20px; margin: 0 auto;background-color: #255184; ">
                            </div>
                            <div style="width: 80%;margin: 0 auto">
                                <div style="overflow: hidden;">
                                    <div style="width: 100%;overflow: hidden; float: right">
                                        <div style="width: 100%">
                                            <br>
                                            <h4 style="color: #77b31be0;"><strong>Mailing Address</strong></h4>
                                            <i class="fa fa-map-marker" aria-hidden="true"></i><span style="margin-left: 26px;"></span><i><?php echo $userInfo->present_address?></i><br>
                                            <i class="fa fa-envelope-o" aria-hidden="true"></i><span style="margin-left: 20px;"></span><i><?php echo $_SESSION['email']?></i><br>
                                            <i class="fa fa-phone" aria-hidden="true"></i><span style="margin-left: 24px;"></span><i><?php echo $userInfo->contact?></i><br>
                                        </div>
                                        <div style="width: 100%">
                                            <br>
                                            <h4 style="color:#77b31be0"><strong>Objective</strong></h4>
                                            <p><?php
                                                if(isset($_SESSION['edit_objective']) && !empty($_SESSION['edit_objective'])){
                                                    echo $_SESSION['edit_objective'];
                                                    $_SESSION['edit_objective']="";
                                                }
                                                else{
                                                    echo $oneCategoryData->objective;
                                                }
                                                ?>
                                            </p>
                                        </div>
                                        <div style="width: 100%">
                                            <br>
                                            <h4 style="color:#77b31be0"><strong>Education</strong></h4>
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <td><strong>Degree</strong></td>
                                                    <td><strong>Institute</strong></td>
                                                    <td><strong>Department/group</strong></td>
                                                    <td><strong>Board</strong></td>
                                                    <td><strong>Result</strong></td>
                                                    <td><strong>Passing Year</strong></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($academic as $oneData){
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $oneData->degree?></td>
                                                        <td><?php echo $oneData->institute?></td>
                                                        <td><?php echo $oneData->department?></td>
                                                        <td><?php echo $oneData->board?></td>
                                                        <td><?php echo $oneData->cgpa ."(".$oneData->out_of.")"?></td>
                                                        <td><?php echo $oneData->passing_year?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>

                                        </div>

                                        <div style="width: 100%">
                                            <br>
                                            <h4 style="color:#77b31be0"><strong>Personal Information</strong></h4>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style="width:30%">Name</td>
                                                    <td style="width:10%">:</td>
                                                    <td style="width:60%"><?php echo $userInfo->full_name?></td>
                                                </tr>
                                                <tr>
                                                    <td>Mother’s Name</td>
                                                    <td>:</td>
                                                    <td><?php echo $userInfo->mother_name?></td>
                                                </tr>
                                                <tr>
                                                    <td>Father’s Name</td>
                                                    <td>:</td>
                                                    <td><?php echo $userInfo->father_name?></td>
                                                </tr>
                                                <tr>
                                                    <td>Present Address</td>
                                                    <td>:</td>
                                                    <td><?php echo $userInfo->present_address?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Permanent Address</td>
                                                    <td>:</td>
                                                    <td><?php echo $userInfo->permanent_address?>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Gender</td>
                                                    <td>:</td>
                                                    <td><?php echo $userInfo->gender?></td>
                                                </tr>
                                                <tr>
                                                    <td>Religion</td>
                                                    <td>:</td>
                                                    <td><?php echo $userInfo->religion?></td>
                                                </tr>
                                                <tr>
                                                    <td>National ID no</td>
                                                    <td>:</td>
                                                    <td><?php echo $userInfo->n_id?></td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>

                                        <div style="width: 100%">
                                            <h4 style="color:#77b31be0"><strong>Language Communication</strong></h4>
                                            <table class="table table-bordered">
                                                <tbody>
                                                <tr>
                                                    <td>Language</td>
                                                    <td style="width: 30% ;">Performance</td>
                                                    <td style="width: 45% ;">Written/Spoken</td>
                                                </tr>
                                                <?php
                                                foreach ($allLanguage as $oneData){
                                                    ?>

                                                    <tr>
                                                        <td style="width:60%"><?php echo $oneData->language?></td>
                                                        <td style="width:10%"><?php echo $oneData->performance?></td>
                                                        <td style="width:50%"><?php echo $oneData->critera?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style="width: 100%">
                                            <h4 style="color:#77b31be0"><strong>Computer Literacy</strong></h4>
                                            <p><?php echo $oneLiteracy->literacy?></p>
                                        </div>
                                        <div style="width: 100%">
                                            <h4 style="color:#77b31be0"><strong>Hobbies</strong></h4>
                                            <p><?php echo $oneHobbie->hobbies?></p>
                                        </div>
                                        <div style="width: 100%">
                                            <?php
                                            if($exist_field){
                                                foreach ($allNewField as $oneData){
                                                    ?>
                                                    <h4 style="color:#77b31be0"><strong><?php echo $oneData->title?></strong></h4>
                                                    <p style="text-align: center"><?php echo $oneData->content?></p>
                                                    <?php
                                                }
                                            }

                                            ?>
                                        </div>
                                        <div style="width: 100%">
                                            <h4 style="color:#77b31be0"><strong>Proclamation</strong></h4>
                                            <p><?php
                                                if(isset($_SESSION['edit_proclamation']) && !empty($_SESSION['edit_proclamation'])){
                                                    echo $_SESSION['edit_proclamation'];
                                                    $_SESSION['edit_proclamation']="";
                                                }
                                                else{
                                                    echo $oneCategoryData->proclamation;
                                                }
                                                ?>
                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div style="width: 100%;overflow: hidden">
                                    <br><br><br>
                                    <br><br><br>
                                    <p>Signature:_____________________________</p>
                                    <p style="margin-left: 75px"><?php echo $userInfo->full_name?></p>
                                    <br><br><br>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div><!--//content-->
        <div style="width: 20%;overflow: hidden;float: right;background: #080058;border-radius: 10px;margin-top: 30px;color: white;margin-right: 70px">

            <h4 class="text-center">Choose Template</h4>
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template1.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template1.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 1</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template2.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template2.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template2.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 2</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template3.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template3.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template3.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 3</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template4.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template4.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template4.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 4</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template5.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template5.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template5.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 5</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template6.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template6.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template6.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 6</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template7.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template7.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template7.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 7</h5></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div style="height: 50px"> </div>
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>
<?php include("templateLayout/script/templateScript.php");?>
<script>
    function ExportPdf(){
        kendo.drawing
            .drawDOM("#myCanvas",
                {
                    paperSize: "A4",
                    margin: { top: "0cm", bottom: "0cm" },
                    scale: 0.8,
                    height: 500
                })
            .then(function(group){
                kendo.drawing.pdf.saveAs(group, "<?php echo $userInfo->full_name?>.pdf")
            });
    }
</script>

</body>
</html>

