<?php
require_once __DIR__ . '../vendor/autoload.php';
session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");

$html=<<<EOD

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>CV GENERATOR</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
   
        <div style="width: 100%;margin: 0 auto;font-family: 'Century Gothic'">
                    <div style="width:30%;background-color: #6D669C;float: left">
                        <div style="margin: 0 auto;width: 90%">
                            <img src="resources/photos/quiyum.jpg" width="100%" style="margin-top: 10px;border-radius: 5px">
                        </div>
                        <h4 class="text-center" style="color: #8DB3E2;font-weight: 600">Abdullah Al Quiyum</h4>
                        <div style="margin-top: 150px">
                            <p style="text-align: center; color: white;font-size: 14px;">Mailing Address</p>
                            <p style="text-align: center; color: black">1st Floor, Nurer Nesa Bhaban,
                                156 CDA Avenue,
                                East Nasirabad, Chittagong</p>

                            <p style="text-align: center;color: black">+88-01826-132308</p>
                            <p style="text-align: center;color: black">Quiyum.nyn@gmail.com</p>
                        </div>
                        <div style="margin-top: 55px">
                            <p style="text-align: center; color: white;font-size: 14px;">Career Objective</p>
                            <p style="text-align: center; color: black">As a beginner in the field of banking, I would be making the best use of my analytical and logical reasoning skills, which would be required to do perform the job efficiently. My job would involve IT, web development, cash flow management, operating and working capital, audits and compliance.</p>
                        </div>

                    </div>
                    <div style="width:70%;background-color: #E6E7E2;float: right ">
                        <div style="margin-top: 10px">
                            <h4 class="text-center" style="color: #5F497A;font-weight: 600">Personal Information</h4>
                        </div>
                        <table style="width:80%;margin: 0 auto">
                            <tbody>
                                <tr>
                                    <td style="width:30%">Name</td>
                                    <td style="width:10%">:</td>
                                    <td style="width:60%">Md Abdullah Al Quiyum</td>
                                </tr>
                                <tr>
                                    <td>Father’s Name</td>
                                    <td>:</td>
                                    <td>Hazi Md Abu Sayed</td>
                                </tr>
                                <tr>
                                    <td>Mother’s Name</td>
                                    <td>:</td>
                                    <td>Jainab Begum</td>
                                </tr>
                                <tr>
                                    <td>Present Address</td>
                                    <td>:</td>
                                    <td>C/O – Lukman Villa, Vill – South
                                        Holain, P/O – Yeakubdandy, P/S: Patiya, District – Chittagong
                                    </td>
                                </tr>
                                <tr>
                                    <td>Permanent Address</td>
                                    <td>:</td>
                                    <td>C/O – Lukman Villa, Vill – South
                                        Holain, P/O – Yeakubdandy, P/S: Patiya, District – Chittagong
                                    </td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>:</td>
                                    <td>10th October, 1992</td>
                                </tr>
                                <tr>
                                    <td>Marital Status</td>
                                    <td>:</td>
                                    <td>Single</td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>:</td>
                                    <td>Male</td>
                                </tr>
                                <tr>
                                    <td>Religion</td>
                                    <td>:</td>
                                    <td>Islam</td>
                                </tr>
                                <tr>
                                    <td>Nationality</td>
                                    <td>:</td>
                                    <td>Bangladeshi</td>
                                </tr>
                                <tr>
                                    <td>National ID no</td>
                                    <td>:</td>
                                    <td>19921516139000090</td>
                                </tr>

                            </tbody>
                        </table>
                        <div style="margin-top: 30px">
                            <h4 class="text-center" style="color: #5F497A;font-weight: 600">Academic Credentials</h4>
                        </div>
                        <div>
                            <table border="1px black solid" style="border-collapse:collapse;width:80%;margin: 0 auto">
                                <thead>
                                    <tr>
                                        <th>Degree</th>
                                        <th>Institute</th>
                                        <th>Department/Group</th>
                                        <th>Result</th>
                                        <th>Passing Year</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Bachelor of Science</td>
                                    <td>Port City International University</td>
                                    <td>Computer Science & Engineering</td>
                                    <td>3.44 (out of 4)</td>
                                    <td>2017</td>v 
                                </tr>
                                <tr>
                                    <td>Diploma in Engineering</td>
                                    <td>Bangladesh Sweden Polytechnic
                                        Institute
                                    </td>
                                    <td>Computer Technology</td>
                                    <td>3.22 (out of 4)</td>
                                    <td>2013</td>
                                </tr>
                                <tr>
                                    <td>Secondary School Certificate</td>
                                    <td>Chittagong Ideal High School</td>
                                    <td>Science</td>
                                    <td>4.00 (out of 5)</td>
                                    <td>2008</td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
</body>
</html>


EOD;


$mpdf = new \Mpdf\Mpdf();
$mpdf->SetDisplayMode('fullwidth');

// Write some HTML code:

$mpdf->WriteHTML($html);


// Output a PDF file directly to the browser
$mpdf->Output("cv_generator.pdf",'D');