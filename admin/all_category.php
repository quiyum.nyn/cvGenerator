<?php
session_start();
require_once("../vendor/autoload.php");
include("../templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\Category;
$object=new Category();
$allData=$object->allCategory();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include("../templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">All Category</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">All Category</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                    }

                    ?>
                    <div class="col-md-12 col-sm-12  col-xs-12 ">
                        <article class="contact-form col-md-12 col-sm-12 col-xs-12  page-row">
                            <?php
                            $sl=1;
                                foreach ($allData as $oneData){
                                    ?>
                                    <div class="col-md-3">
                                        <div class="thumbnail">
                                            <img src="../resources/category_pic/<?php echo $oneData->pic?>" class="img-responsive">
                                        </div>
                                        <div class="caption">
                                            <h5 style="text-align: center"><?php echo $oneData->category_name?></h5>
                                        </div>

                                            <center>
                                                <a href="edit_category.php?category_id=<?php echo $oneData->id?>" class="btn btn-primary">Edit</a>
                                                <a href="edit_category_pic.php?category_id=<?php echo $oneData->id?>" style="background-color: #23544d;border-color: #23544d;" class="btn btn-primary">Change Picture</a>
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="<?php echo '#modal'.$sl?>" style="background-color: #ef110a;border-color: #ef110a;">Delete</button>
                                            </center>
                                        <div id="<?php echo 'modal'.$sl?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Delete This Category</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="col-md-12">
                                                            <form action="../controller/deleteCategory.php" method="post">
                                                                <p>Are you sure to delete <?php echo $oneData->category_name?>?</p>
                                                                <input type="hidden" name="category_id" value="<?php echo $oneData->id?>" readonly>
                                                                <input type="submit" class="btn btn-primary" value="Delete">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                            <?php
                                    $sl++;
                                }
                            ?>
                        </article><!--//contact-form-->
                    </div>

                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("../templateLayout/footer.php");?>

<?php include("../templateLayout/script/templateScript.php");?>

</body>
</html>

