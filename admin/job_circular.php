<?php
session_start();
require_once("../vendor/autoload.php");
include("../templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include("../templateLayout/css/templateCss.php");?>
    <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Add Job</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Add Job</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                    }

                    ?>
                    <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2 col-xs-12 col-xs-offset-0">
                        <article class="contact-form col-md-12 col-sm-12 col-xs-12  page-row">
                            <form action="../controller/add_job.php" method="post">
                                <div class="form-group name">
                                    <label>Title </label>
                                    <input type="text" class="form-control" placeholder="Job Title" name="title"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label>Company Name </label>
                                    <input type="text" class="form-control" placeholder="Company Name" name="company_name"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label>Company Contact </label>
                                    <input type="number" class="form-control" placeholder="Company Contact " name="company_contact"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label>Company Address </label>
                                    <input type="text" class="form-control" placeholder="Company Address " name="company_address"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label>Position</label>
                                    <input type="text" class="form-control" placeholder="Position" name="post"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label>vacancy</label>
                                    <input type="text" class="form-control" placeholder="Number of vacancy" name="vacancy"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label>Link</label>
                                    <input type="text" class="form-control" placeholder="Job web link" name="link"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label>Deadline</label>
                                    <input type="text" class="form-control date_field" placeholder="Deadline" name="deadline"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label>Requirement</label>
                                    <textarea id="text" rows="5" name="requirement" class="form-control" placeholder="Job Requirement" required></textarea>
                                </div><!--//form-group-->
                                
                                <button type="submit" class="btn btn-theme" >Add Job</button>
                            </form>
                        </article><!--//contact-form-->
                    </div>

                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("../templateLayout/footer.php");?>

<?php include("../templateLayout/script/templateScript.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
<script> CKEDITOR.replace( 'text' );</script>
</body>
</html>

