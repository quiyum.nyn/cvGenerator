<?php
namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;
class Hobbies extends Database
{
    public $id;
    public $user_id;
    public $hobbies;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('user_id',$data)){
            $this->user_id=$data['user_id'];
        }
        if(array_key_exists('hobbies',$data)){
            $this->hobbies=$data['hobbies'];
        }
        return $this;
    }


    public function insertData(){
        $query="INSERT INTO `hobbies` (`id`, `user_id`, `hobbies`) VALUES (?,?,?)";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->id);
        $STH->bindParam(2,$this->user_id);
        $STH->bindParam(3,$this->hobbies);
        $STH->execute();
    }
    public function isInserted(){
        $query = "SELECT * FROM `hobbies` WHERE `user_id`='$this->user_id'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function showHobbie(){
        $sql = "SELECT * FROM `hobbies` WHERE `user_id`='$this->user_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function updateData(){
        $query= "UPDATE hobbies SET hobbies=? WHERE user_id='$this->user_id'";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->hobbies);
        $STH->execute();

    }
    public function deleteData(){
        $query="DELETE FROM hobbies WHERE id='$this->id'";
        $this->DBH->exec($query);

    }
}