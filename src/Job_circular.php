<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 12/1/2017
 * Time: 4:54 PM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Job_circular extends Database
{
    public $id;
    public $title;
    public $company_name;
    public $company_contact;
    public $company_address;
    public $post;
    public $requirement;
    public $link;
    public $deadline;
    public $post_date;
    public $status;
    public $vacancy;

    public function __construct()
    {
        parent::__construct();
    }
    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('title',$data)){
            $this->title=$data['title'];
        }
        if(array_key_exists('post',$data)){
            $this->post=$data['post'];
        }
        if(array_key_exists('requirement',$data)){
            $this->requirement=$data['requirement'];
        }
        if(array_key_exists('link',$data)){
            $this->link=$data['link'];
        }
        if(array_key_exists('deadline',$data)){
            $this->deadline=$data['deadline'];
        }
        if(array_key_exists('company_name',$data)){
            $this->company_name=$data['company_name'];
        }
        if(array_key_exists('company_contact',$data)){
            $this->company_contact=$data['company_contact'];
        }
        if(array_key_exists('company_address',$data)){
            $this->company_address=$data['company_address'];
        }
        if(array_key_exists('vacancy',$data)){
            $this->vacancy=$data['vacancy'];
        }

    }

    public function store(){

        $query="INSERT INTO `job_circular`(`title`,`company_name`,`company_contact`,`company_address`,`post`,`requirement`,`vacancy`,`link`,`deadline`) VALUES (?,?,?,?,?,?,?,?,?)";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->title);
        $STH->bindParam(2,$this->company_name);
        $STH->bindParam(3,$this->company_contact);
        $STH->bindParam(4,$this->company_address);
        $STH->bindParam(5,$this->post);
        $STH->bindParam(6,$this->requirement);
        $STH->bindParam(7,$this->vacancy);
        $STH->bindParam(8,$this->link);
        $STH->bindParam(9,$this->deadline);
        $STH->execute();
    }
    public function update(){

        $query="UPDATE `job_circular` SET `title`=?,`company_name`=?,`company_contact`=?,`company_address`=?,`post`=?,`requirement`=?,`vacancy`=?,`link`=?,`deadline`=? WHERE id='$this->id'";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->title);
        $STH->bindParam(2,$this->company_name);
        $STH->bindParam(3,$this->company_contact);
        $STH->bindParam(4,$this->company_address);
        $STH->bindParam(5,$this->post);
        $STH->bindParam(6,$this->requirement);
        $STH->bindParam(7,$this->vacancy);
        $STH->bindParam(8,$this->link);
        $STH->bindParam(9,$this->deadline);
        $STH->execute();
    }
    public function show(){
        $sql = "SELECT * FROM `job_circular` WHERE status='0'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOne(){
        $sql = "SELECT * FROM `job_circular` WHERE id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function delete(){
        $this->status=1;
        $query="UPDATE `job_circular` SET `status`=? WHERE id='$this->id'";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }
}