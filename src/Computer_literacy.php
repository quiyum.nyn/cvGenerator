<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 10/19/2017
 * Time: 5:16 PM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;


class Computer_literacy extends Database
{
    public $id;
    public $user_id;
    public $literacy;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('user_id',$data)){
            $this->user_id=$data['user_id'];
        }
        if(array_key_exists('literacy',$data)){
            $this->literacy=$data['literacy'];
        }
        return $this;
    }


    public function insertData(){
        $query="INSERT INTO `computer_literacy` (`id`, `user_id`, `literacy`) VALUES (?,?,?)";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->id);
        $STH->bindParam(2,$this->user_id);
        $STH->bindParam(3,$this->literacy);
        $STH->execute();
    }
    public function showLiteracy(){
        $sql = "SELECT * FROM `computer_literacy` WHERE `user_id`='$this->user_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function isInserted(){
        $query = "SELECT * FROM `computer_literacy` WHERE `user_id`='$this->user_id'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function updateData(){
        $query= "UPDATE computer_literacy SET literacy=? WHERE user_id='$this->user_id'";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->literacy);
        $STH->execute();

    }
    public function deleteData(){
        $query="DELETE FROM computer_literacy WHERE id='$this->id'";
        $this->DBH->exec($query);

    }
}