<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 10/19/2017
 * Time: 4:23 PM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Achievement extends Database
{
    public $id;
    public $user_id;
    public $title;
    public $details;
    public $achieve_id;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('user_id',$data)){
            $this->user_id=$data['user_id'];
        }
        if(array_key_exists('new_title',$data)){
            $this->title=$data['new_title'];
        }
        if(array_key_exists('new_details',$data)){
            $this->details=$data['new_details'];
        }

        if(array_key_exists('edit_achievement_id',$data)){
            $this->achieve_id=$data['edit_achievement_id'];
        }
        
        return $this;
    }

    public function insertData(){
        $title=explode("]]}",$this->title);
        $details=explode("]]}",$this->details);
        $count=0;
        foreach($title as $pr){
            $query= "INSERT INTO `achievement`(`user_id`, `achievement_title`, `achievement_from`) VALUES (?,?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$this->user_id);
            $STH->bindParam(2,$title[$count]);
            $STH->bindParam(3,$details[$count]);
            $STH->execute();
            $count++;
        }
    }
    public function updateData(){
            $query= "UPDATE achievement SET achievement_title=?, achievement_from=? WHERE id='$this->id'";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$this->title);
            $STH->bindParam(2,$this->details);
            $STH->execute();

    }
    public function isInserted(){
        $query = "SELECT * FROM `achievement` WHERE `user_id`='$this->user_id'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function showAcieve(){
        $sql = "SELECT * FROM `achievement` WHERE `user_id`='$this->user_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOneAcieve(){
        $sql = "SELECT * FROM `achievement` WHERE `id`='$this->achieve_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function deleteData(){
        $query="DELETE FROM achievement WHERE id='$this->id'";
        $this->DBH->exec($query);

    }
}
