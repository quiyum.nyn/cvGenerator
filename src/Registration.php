<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 8/21/2017
 * Time: 2:20 AM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Registration extends Database
{
    public $id;
    public $name;
    public $password;
    public $reg_date;
    public $email;

    public function __construct()
    {
        parent::__construct();
    }
    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        return $this;
    }


    public function insertData(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->reg_date=$date;
        $query="INSERT INTO registration (name,email,password,reg_date) VALUES (?,?,?,?)";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->name);
        $STH->bindParam(2,$this->email);
        $STH->bindParam(3,$this->password);
        $STH->bindParam(4,$this->reg_date);
        $STH->execute();
    }
    public function loginCheck(){
        $query = "SELECT * FROM `registration` WHERE `email`='$this->email' AND `status`='0' AND `password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function loginCheckAdmin(){
        $query = "SELECT * FROM `registration` WHERE `email`='$this->email' AND `status`='1' AND `password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function userId(){
        $sql = "SELECT id,name FROM `registration` WHERE `email`='$this->email' AND `status`='0' AND `password`='$this->password'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function userIdAdmin(){
        $sql = "SELECT id,name FROM `registration` WHERE `email`='$this->email' AND `status`='1' AND `password`='$this->password'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function log_out(){
        $_SESSION['email']="";
        return TRUE;
    }
    public function logged_in(){
        if ((array_key_exists('email', $_SESSION)) && (!empty($_SESSION['email']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}