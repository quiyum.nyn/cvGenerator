<?php
include_once "../vendor/autoload.php";
use App\New_field;
use App\Utility\Utility;
use App\Message\Message;
$object=new New_field();
if($_POST['field']=='achievement' || $_POST['field']=='category' || $_POST['field']=='computer_literacy' || $_POST['field']=='hobbies' || $_POST['field']=='language_communication' || $_POST['field']=='registration' || $_POST['field']=='user_info' || $_POST['field']=='education_qualification'){
    Message::setMessage("You can't add this category");
    return Utility::redirect('../profile.php'); 
}
else{
    $object->prepareData($_POST);
    $object->newField();
    Message::setMessage("Field Added!");
    return Utility::redirect('../profile.php');
}

