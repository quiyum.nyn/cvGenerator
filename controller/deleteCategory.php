<?php
include_once "../vendor/autoload.php";
use App\Category;
use App\Utility\Utility;
$object=new Category();
$object->prepareData($_POST);
$object->deleteData();
return Utility::redirect('../admin/all_category.php');
