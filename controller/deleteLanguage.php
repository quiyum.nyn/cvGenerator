<?php
include_once "../vendor/autoload.php";
use App\Language_communication;
use App\Utility\Utility;
$object=new Language_communication();
$object->prepareData($_GET);
$object->deleteData();
return Utility::redirect('../profile.php');
