<?php
include_once "../vendor/autoload.php";
use App\Hobbies;
use App\Utility\Utility;
$object=new Hobbies();
$object->prepareData($_POST);
$object->updateData();
return Utility::redirect($_SERVER['HTTP_REFERER']);
