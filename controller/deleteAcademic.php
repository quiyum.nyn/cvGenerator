<?php
include_once "../vendor/autoload.php";
use App\Education_qualification;
use App\Utility\Utility;
$object=new Education_qualification();
$object->prepareData($_GET);
$object->deleteData();
return Utility::redirect('../profile.php');
