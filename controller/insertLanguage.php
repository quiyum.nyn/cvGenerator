<?php
include_once "../vendor/autoload.php";
use App\Language_communication;
use App\Utility\Utility;
$object=new Language_communication();
$_POST['new_language']=implode("]]}",$_POST['language']);
$_POST['new_performance']=implode("]]}",$_POST['performance']);
$_POST['new_criteria']=implode("]]}",$_POST['criteria']);
$object->prepareData($_POST);
$object->insertData();
return Utility::redirect($_SERVER['HTTP_REFERER']);
