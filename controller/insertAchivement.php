<?php
include_once "../vendor/autoload.php";
use App\Achievement;
use App\Utility\Utility;
$object=new Achievement();
$_POST['new_title']=implode("]]}",$_POST['title']);
$_POST['new_details']=implode("]]}",$_POST['details']);
$object->prepareData($_POST);
$object->insertData();
return Utility::redirect($_SERVER['HTTP_REFERER']);
