<?php
include_once "../vendor/autoload.php";
use App\Education_qualification;
use App\Utility\Utility;
$object=new Education_qualification();
$_POST['new_degree']=implode("]]}",$_POST['degree']);
$_POST['new_institute']=implode("]]}",$_POST['institute']);
$_POST['new_department']=implode("]]}",$_POST['department']);
$_POST['new_board']=implode("]]}",$_POST['board']);
$_POST['new_cgpa']=implode("]]}",$_POST['cgpa']);
$_POST['new_out_of']=implode("]]}",$_POST['out_of']);
$_POST['new_passing_year']=implode("]]}",$_POST['passing_year']);
$object->prepareData($_POST);
$object->insertData();
return Utility::redirect('../profile.php');
