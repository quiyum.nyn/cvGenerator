<?php
include_once "../vendor/autoload.php";
use App\Category;
use App\Utility\Utility;
use App\Message\Message;
$object=new Category();
$object->prepareData($_POST);
$object->insertData();
Message::setMessage("Category added successfully");
return Utility::redirect('../admin/all_category.php');
