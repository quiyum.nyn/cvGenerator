<?php
include_once "../vendor/autoload.php";
use App\Category;
use App\Utility\Utility;
use App\Message\Message;
$object=new Category();
$object->prepareData($_POST);
$object->update();
Message::setMessage("Category updated successfully");
return Utility::redirect('../admin/all_category.php');
