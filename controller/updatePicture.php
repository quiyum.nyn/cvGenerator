<?php
include_once "../vendor/autoload.php";
use App\User_info;
use App\Utility\Utility;
$object=new User_info();
$object->prepareData($_POST);
$object->updatePicture();
return Utility::redirect($_SERVER['HTTP_REFERER']);
