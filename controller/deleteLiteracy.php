<?php
include_once "../vendor/autoload.php";
use App\Computer_literacy;
use App\Utility\Utility;
$object=new Computer_literacy();
$object->prepareData($_GET);
$object->deleteData();
return Utility::redirect('../profile.php');
