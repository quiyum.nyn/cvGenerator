<?php
include_once "../vendor/autoload.php";
use App\Category;
use App\Utility\Utility;
use App\Message\Message;
$object=new Category();
$object->prepareData($_POST);
$object->updatePic();
Message::setMessage("Picture Change successfully");
return Utility::redirect('../admin/all_category.php');
