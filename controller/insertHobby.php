<?php
include_once "../vendor/autoload.php";
use App\Hobbies;
use App\Utility\Utility;
$object=new Hobbies();
$object->prepareData($_POST);
$object->insertData();
return Utility::redirect($_SERVER['HTTP_REFERER']);
