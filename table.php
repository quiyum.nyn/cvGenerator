
<?php
session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\New_Category;
$object=new New_Category();
$allData=$object->showTable();
var_dump($allData);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container" style="width: 800px">
        <form action="table.php" method="post">
            <input type="text" name="product" list="productName"/>
            <datalist id="productName">
                <?php
                foreach ($allData as $oneData){
                    ?>
                    <option value="<?php echo $oneData->Tables_in_cv_generator?>"><?php echo $oneData->Tables_in_cv_generator?></option>
                    <?php
                }
                ?>
            </datalist>
            <input type="submit">
        </form>


    </div><!--//content-->
    <div style="height: 50px"> </div>
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>




</body>
</html>

