<?php
session_start();
include("templateLayout/templateInformation.php");
include_once "vendor/autoload.php";
use App\Message\Message;
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>
    <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Login</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Login</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                    }

                    ?>
                    <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12 col-xs-offset-0">
                        <article class="contact-form col-md-12 col-sm-12 col-xs-12  page-row">
                            <form action="controller/login.php" method="post">
                                <h5 style="color: #2e6da4"><b>Login</b></h5>
                                <div class="form-group name">
                                    <label for="name">Select Status </label>
                                   <select name="login_status" class="form-control" required>
                                       <option value="0">Admin</option>
                                       <option value="1">User</option>
                                   </select>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label for="name">Email </label>
                                    <input id="name" type="email" class="form-control" placeholder="Enter your email" name="email"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label for="name">Password </label>
                                    <input id="name" type="password" class="form-control" minlength="6" placeholder="Enter Your Password " name="password"  required>
                                </div><!--//form-group-->
                                <button type="submit" class="btn btn-theme" >Login</button>
                            </form>
                        </article><!--//contact-form-->
                    </div>

                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>

<?php include("templateLayout/script/templateScript.php");?>



</body>
</html>

