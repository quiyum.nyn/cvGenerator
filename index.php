<?php
session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>
   
</head> 

<body class="home-page">
    <div class="wrapper">
        <!-- ******HEADER****** -->
        <?php include("templateLayout/headerAndNavigation.php");?>
        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div id="promo-slider" class="slider flexslider">
                <ul class="slides">
                    <li>
                        <img src="resources/assets/images/slides/3.jpg" alt="" />
                        <p class="flex-caption">
                            <span class="main" > Make your CV</span>
                            <br />
                            <span class="secondary clearfix" >Online CV Generator</span>
                        </p>
                    </li>
                    <li>
                        <img src="resources/assets/images/slides/2.jpg" alt="" />
                        <p class="flex-caption">
                            <span class="main" > Make your CV</span>
                            <br />
                            <span class="secondary clearfix" >Online CV Generator</span>
                        </p>
                    </li>
                    <li>
                        <img src="resources/assets/images/slides/5.jpg" alt="" />
                        <p class="flex-caption">
                            <span class="main" > Make your CV</span>
                            <br />
                            <span class="secondary clearfix" >Online CV Generator</span>
                        </p>
                    </li>
                    <li>
                        <img src="resources/assets/images/slides/4.jpg" alt="" />
                        <p class="flex-caption">
                            <span class="main" > Make your CV</span>
                            <br />
                            <span class="secondary clearfix" >Online CV Generator</span>
                        </p>
                    </li>
                </ul><!--//slides-->
            </div><!--//flexslider-->
            <section class="promo box box-dark">        
                <div class="col-md-9">
                <h4 class="section-heading">Create Profile</h4>
                    <p>After Completing your profile ready, then you can generate your CV</p>
                </div>  
                <div class="col-md-3">
                    <a class="btn btn-cta" href="profile.php"><i class="fa fa-play-circle"></i>Apply Now</a>
                </div>
            </section><!--//promo-->
            <div class="row cols-wrapper">
                <div class="col-md-9">
                    <section class="links">
                        <h1 class="section-heading text-highlight"><span class="line">Latest Templates</span></h1>
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <img src="resources/img/template.jpg" class="img-responsive img-rounded" alt="template">
                                    <h5 class="caption text-center">Template 1</h5>
                                </div>
                              <div class="col-md-4 col-sm-4 col-xs-6">
                                  <img src="resources/img/template2.jpg" class="img-responsive img-rounded" alt="template">
                                  <h5 class="caption text-center">Template 2</h5>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-6">
                                  <img src="resources/img/template3.jpg" class="img-responsive img-rounded" alt="template">
                                  <h5 class="caption text-center">Template 3</h5>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-6">
                                  <img src="resources/img/template4.jpg" class="img-responsive img-rounded" alt="template">
                                  <h5 class="caption text-center">Template 4</h5>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-6">
                                  <img src="resources/img/template5.jpg" class="img-responsive img-rounded" alt="template">
                                  <h5 class="caption text-center">Template 5</h5>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-6">
                                  <img src="resources/img/template6.jpg" class="img-responsive img-rounded" alt="template">
                                  <h5 class="caption text-center">Template 6</h5>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-6">
                                  <img src="resources/img/template7.jpg" class="img-responsive img-rounded" alt="template">
                                  <h5 class="caption text-center">Template 7</h5>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-6">
                                  <img src="resources/img/template6.jpg" class="img-responsive img-rounded" alt="template">
                                  <h5 class="caption text-center">Template 6</h5>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-6">
                                  <img src="resources/img/template7.jpg" class="img-responsive img-rounded" alt="template">
                                  <h5 class="caption text-center">Template 7</h5>
                              </div>
                          </div>
                        </div>
                    </section><!--//links-->


                </div>
                <div class="col-md-3">
                    <section class="testimonials">
                        <div class="carousel-controls">
                            <a class="prev" href="#testimonials-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
                            <a class="next" href="#testimonials-carousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
                        </div><!--//carousel-controls-->
                        <div class="section-content" style="margin-top: 30px">
                            <div id="testimonials-carousel" class="testimonials-carousel carousel slide">
                                <div class="carousel-inner" style="min-height: 515px">
                                    <div class="item active">
                                        <blockquote class="quote">
                                            <p style="font-size: 16px;font-weight: 600;color: #5c4d80">Only 2% of applicants get the interview.</p>
                                            <p><i class="fa fa-quote-left"></i>First impressions matter when competing for the job you want. Recruiters spend less than 6 seconds deciding whether your resume or CV is worth a second look.</p>
                                                <br><p>VisualCV guarantees you will always make the right first impression. Choose from a variety of industry-approved templates, create multiple profile versions, and track the results. You can even edit your CV on your mobile device.
                                               </p><br><p> Make your next move with confidence.</p>
                                        </blockquote>                

                                    </div><!--//item-->
                                    <div class="item">
                                        <blockquote class="quote">
                                            <p><i class="fa fa-quote-left"></i>
                                                As a hiring manager, I see dozens of poorly designed resumes every day. When I see a well designed CV, I immediately give them a call even if they don't quite have the experience. It shows they are excited to work for my company, which matters more than years of experience.</p>
                                        </blockquote>
                                        <div class="row">
                                            <p class="people col-md-8 col-sm-3 col-xs-8"><span class="name">Matt Smith</span><br /><span class="title"> COO <br>Later.com</span></p>
                                        </div>
                                    </div><!--//item-->
                                   
                                    
                                </div><!--//carousel-inner-->
                            </div><!--//testimonials-carousel-->
                        </div><!--//section-content-->
                    </section><!--//testimonials-->
                    <section class="links" style="min-height: 157px">
                        <h1 class="section-heading text-highlight"><span class="line">Quick Links</span></h1>
                        <div class="section-content">
                            <p><a href=""><i class="fa fa-caret-right"></i>Home</a></p>
                            <p><a href="profile.php"><i class="fa fa-caret-right"></i>Profile</a></p>
                            <p><a href="generate_cv.php"><i class="fa fa-caret-right"></i>Generate CV</a></p>
                        </div><!--//section-content-->
                    </section><!--//links-->

                </div><!--//col-md-3-->
            </div><!--//cols-wrapper-->

        </div><!--//content-->
    </div><!--//wrapper-->
    
    <!-- ******FOOTER****** -->
    <?php include("templateLayout/footer.php");?>


    <?php include("templateLayout/script/templateScript.php");?>
</body>
</html> 

