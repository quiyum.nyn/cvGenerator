<?php
session_start();
require_once "vendor/autoload.php";
include("templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\User_info;
$object=new User_info();
$object->prepareData($_SESSION);
$exist_userInfo=$object->isInserted();
$complete=0;
if($exist_userInfo){
    $userInfo=$object->showUserInfo();
}
use App\Education_qualification;
$objEdu=new Education_qualification();
$objEdu->prepareData($_SESSION);
$exist_academic=$objEdu->isInserted();
if($exist_academic){
    $academic=$objEdu->showAcademic();
}
use App\Computer_literacy;
$objLiteracy=new Computer_literacy();
$objLiteracy->prepareData($_SESSION);
$exist_literacy=$objLiteracy->isInserted();

use App\Hobbies;
$objHobbiy=new Hobbies();
$objHobbiy->prepareData($_SESSION);
$exist_hobbie=$objHobbiy->isInserted();
if($exist_hobbie){
    $oneHobbie=$objHobbiy->showHobbie();
}


use App\Language_communication;
$languageObj=new Language_communication();
$languageObj->prepareData($_SESSION);
$existLanguage=$languageObj->isInserted();

if($existLanguage){
    if(isset($_GET['language_id'])){
        $languageObj->prepareData($_GET);
        $oneLanguage=$languageObj->showOneData();
    }
    else{
        $allLanguage=$languageObj->showLanguage();
    }

}
if($exist_literacy){
    $oneLiteracy=$objLiteracy->showLiteracy();
}
if(isset($_GET['update_academic_id'])){
    $objEdu->prepareData($_GET);
    $oneEdu=$objEdu->showOneAcademic();
}


$infovalue=0;
$academicvalue=0;
$literacyValue=0;
$languageValue=0;
$hobbieValue=0;
if($exist_userInfo){
    $infovalue=23;
}
if($exist_academic){
    $academicvalue=23;
}
if($exist_literacy){
    $literacyValue=18;
}
if($existLanguage){
    $languageValue=18;
}
if($exist_hobbie){
    $hobbieValue=18;
}
    $complete=$infovalue+$academicvalue+$literacyValue+$languageValue+$hobbieValue;
    $_SESSION['complete']=$complete;

use App\New_field;
$fieldObj=new New_field();
$fieldObj->prepareData($_SESSION);
$exist_field=$fieldObj->isInserted();
if($exist_field){
    $allNewField=$fieldObj->showNew();
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>
    <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="resources/assets/css/thumbnail-gallery.css">
<style>
    .tz-gallery{
        font-family: Agency FB;
    }
    .form-group{
        font-size: 17px;
    }
    .form-control{
        font-size: 17px;
    }
    input[type="file"]{
        font-size: 14px;
    }
    .form-inline .form-group { margin-right:10px; }
    .well-primary {
        color: rgb(255, 255, 255);
        background-color: rgb(66, 139, 202);
        border-color: rgb(53, 126, 189);
    }
    .glyphicon { margin-right:5px; }
</style>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Profile</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Profile</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">
                    <div class="container gallery-container" style="margin-top: -65px">



                        <div class="tz-gallery">
                            <div class="row">
                                <?php
                                if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                    $msg = Message::getMessage();

                                    echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                                }

                                ?>
                                <h3 style="font-family: 'Agency FB';text-align: center"><?php echo $_SESSION['user_name']?></h3>
                                <?php
                                    if($exist_userInfo){
                                        ?>
                                        <center><img  src="resources/user_photos/<?php echo $userInfo->picture?>" width="15%" class="img-responsive"></center>
                                        <form action="profile.php" method="post">
                                            <center><br><input type="submit" class="btn btn-primary" name="change_picture" value="Change Picture"></center>
                                        </form>
                                        <?php
                                    }
                                ?>
                                <div class="col-md-8 col-md-offset-2 ">
                                    <h5 style="font-family: 'Agency FB';text-align: center">Profile Completed: <?php echo $_SESSION['complete']?> %</h5>
                                    <div class="progress">

                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $_SESSION['complete']?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $_SESSION['complete']?>%">
                                            <?php echo $_SESSION['complete']?>%
                                        </div>
                                    </div>
                                    <?php
                                        if($complete==100){
                                           ?>
                                            <center><a href="generate_cv.php" class="btn btn-primary">Generate Your CV</a></center>
                                    <?php
                                        }
                                    ?>

                                    <br>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                            <div class="panel-group" id="accordion">
                                                <?php
                                                if(isset($_POST['change_picture'])){
                                                   ?>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-file">
                                                                </span>Change Picture</a>
                                                            </h4>
                                                        </div>

                                                        <div id="collapseOne" class="panel-collapse collapse in">
                                                            <div class="panel-body">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">Image Upload</div>
                                                                    <div class="panel-body">

                                                                        <div class="row">
                                                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                                                <strong>Select Image:</strong>
                                                                                <br/>
                                                                                <div class="form-group">
                                                                                    <input type="file" class="form-control" id="upload">
                                                                                </div>

                                                                                <br/>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                                                                                <div id="upload-demo" style="width:100%;margin: 0 auto"></div>
                                                                                <button class="btn btn-success upload-result">Crop Image</button>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                                                                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 35px">
                                                                                    <div id="upload-demo-i" style="background:#e1e1e1;width:66%;padding:0px;min-height:300px;margin: 0 auto;"></div>
                                                                                </div>
                                                                            </div>


                                                                        </div>

                                                                    </div>



                                                                </div>
                                                                <form action="controller/updatePicture.php" method="post">
                                                                    <div id="upload-demo-i2"></div>
                                                                    <br>
                                                                    <input type="submit" class="btn btn-primary" value="Upload Picture" style="float: left"/>
                                                                    <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                </form>
                                                                <a href="profile.php" class="btn btn-danger">Cancle to change picture</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                                ?>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-file">
                            </span>Basic Information</a>
                                                        </h4>
                                                    </div>

                                                    <div id="collapseOne" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <?php
                                                                if($exist_userInfo){
                                                                    ?>
                                                                    <div class="row">
                                                                       
                                                                        <form action="controller/updateBasicInfo.php" method="post">
                                                                            <div id="upload-demo-i2"></div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="shorttitle">Name</label>
                                                                                    <input type="text" class="form-control" name="full_name" value="<?php echo $userInfo->full_name?>" required style="background: #cbc9e2;"/>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="shorttitle">Father's Name</label>
                                                                                    <input type="text" class="form-control" id='shorttitle' name="father" value="<?php echo $userInfo->father_name?>" required style="background: #cbc9e2;"/>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Present Address</label>
                                                                                    <input type="text" class="form-control" name="present" value="<?php echo $userInfo->present_address?>" required style="background: #cbc9e2;"/>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="grantparent">Gender</label>
                                                                                    <select class="form-control" id="grantparent" name="gender" style="background: #cbc9e2;" required>
                                                                                        <option>-select gender-</option>
                                                                                        <option value="Male" <?php if($userInfo->gender=="Male"){echo "selected";}?> >Male</option>
                                                                                        <option value="Female" <?php if($userInfo->gender=="Female"){echo "selected";}?> >Female</option>
                                                                                        <option value="Undefined" <?php if($userInfo->gender=="Undefined"){echo "selected";}?> >Undefined</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Contact</label>
                                                                                    <input type="number" class="form-control" name="contact" value="<?php echo $userInfo->contact?>" required style="background: #cbc9e2;"/>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <input type="submit" class="btn btn-primary" value="Update Information"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="grant1">Mother's Name</label>
                                                                                    <input type="text" class="form-control" id='grant1' name="mother" value="<?php echo $userInfo->mother_name?>" required style="background: #cbc9e2;"/>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="title">Permanent Address</label>
                                                                                    <input type="text" class="form-control" id='shorttitle' name="permanent" value="<?php echo $userInfo->permanent_address?>" required style="background: #cbc9e2;"/>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="suplimenta">Religion</label>
                                                                                    <input type="text" class="form-control" name="religion" value="<?php echo $userInfo->religion?>" required style="background: #cbc9e2;"/>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label for="suplimenta">NID No</label>
                                                                                    <input type="number" class="form-control" name="nid" value="<?php echo $userInfo->n_id?>" required style="background: #cbc9e2;"/>
                                                                                </div>
                                                                            </div>
                                                                            <input type="hidden" name="user_id" value="<?php echo $userInfo->user_id?>">
                                                                        </form>

                                                                    </div>
                                                                    <?php
                                                                }
                                                            else{
                                                                ?>

                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">Image Upload</div>
                                                                        <div class="panel-body">

                                                                            <div class="row">
                                                                                <div class="col-md-12 col-xs-12 col-sm-12">
                                                                                    <strong>Select Image:</strong>
                                                                                    <br/>
                                                                                    <div class="form-group">
                                                                                        <input type="file" class="form-control" id="upload">
                                                                                    </div>

                                                                                    <br/>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                                                                                    <div id="upload-demo" style="width:100%;margin: 0 auto"></div>
                                                                                    <button class="btn btn-success upload-result">Croped Image</button>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                                                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 35px">
                                                                                        <div id="upload-demo-i" style="background:#e1e1e1;width:66%;padding:0px;min-height:300px;margin: 0 auto;"></div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>

                                                                        </div>



                                                                    </div>
                                                                    <form action="controller/insertBasicInfo.php" method="post">
                                                                        <div id="upload-demo-i2"></div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="shorttitle">Name</label>
                                                                                <input type="text" class="form-control" name="full_name" placeholder="Full Name"  required />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="shorttitle">Father's Name</label>
                                                                                <input type="text" class="form-control" id='shorttitle' name="father" placeholder="Father's Name" required />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Present Address</label>
                                                                                <input type="text" class="form-control" name="present" placeholder="Present Address" required />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="grantparent">Gender</label>
                                                                                <select class="form-control" id="grantparent" name="gender" required>
                                                                                    <option>-select gender-</option>
                                                                                    <option value="Male">Male</option>
                                                                                    <option value="Female">Female</option>
                                                                                    <option value="Undefined">Undefined</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Contact</label>
                                                                                <input type="number" class="form-control" name="contact" placeholder="Contact" required />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="submit" class="btn btn-primary" value="Submit Information"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="grant1">Mother's Name</label>
                                                                                <input type="text" class="form-control" id='grant1' name="mother" placeholder="Mother's Name" required />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="title">Permanent Address</label>
                                                                                <input type="text" class="form-control" id='shorttitle' name="permanent" placeholder="Permanent Address" required />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="suplimenta">Religion</label>
                                                                                <input type="text" class="form-control" name="religion" placeholder="Religion" required />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="suplimenta">NID No</label>
                                                                                <input type="number" class="form-control" name="nid" placeholder="NID No" required />
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                    </form>

                                                               
                                                            <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-th-list">
                            </span>Academic Credentials</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseTwo" class="panel-collapse collapse <?php if(isset($_GET['update_academic_id'])){echo 'in';}?>">
                                                        <?php
                                                            if($exist_academic){
                                                                ?>
                                                                <?php
                                                                    if(isset($_GET['update_academic_id'])){
                                                                        ?>
                                                                        <div class="row">
                                                                            <form action="controller/updateAcademic.php" method="post">
                                                                                <div class="col-md-12">
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Degree</label>
                                                                                            <input type="text" name="new_degree" class="form-control" value="<?php echo $oneEdu->degree?>" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Institute</label>
                                                                                            <input type="text" name="new_institute" class="form-control" value="<?php echo $oneEdu->institute?>"  required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Department/Group</label>
                                                                                            <input type="text" name="new_department" class="form-control" value="<?php echo $oneEdu->department?>"  required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Board</label>
                                                                                            <input type="text" name="new_board" class="form-control" value="<?php echo $oneEdu->board?>"  required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">CGPA</label>
                                                                                            <input type="text" name="new_cgpa" class="form-control" value="<?php echo $oneEdu->cgpa?>"  required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Out of</label>
                                                                                            <input type="text" name="new_out_of" class="form-control" value="<?php echo $oneEdu->out_of?>"  required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Passign Year</label>
                                                                                            <input type="number" name="new_passing_year" class="form-control" value="<?php echo $oneEdu->passing_year?>"  required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <div class="form-group">
                                                                                            <input type="hidden" name="id" value="<?php echo $oneEdu->id?>">
                                                                                            <input type="submit" class="btn btn-primary" value="Update"/>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <a href="profile.php" class="btn btn-danger">Cancel to update Academic Details</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>

                                                                        </div>

                                                                        <?php
                                                                    }
                                                                else{
                                                                    ?>
                                                                    <table class="table table-responsive table-bordered">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Degree</th>
                                                                            <th>Institute</th>
                                                                            <th>Department/Group</th>
                                                                            <th>Board</th>
                                                                            <th>CGPA</th>
                                                                            <th>Passing Year</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php
                                                                        foreach ($academic as $oneData){
                                                                            ?>
                                                                            <tr>
                                                                                <td><?php echo $oneData->degree?></td>
                                                                                <td><?php echo $oneData->institute?></td>
                                                                                <td><?php echo $oneData->department?></td>
                                                                                <td><?php echo $oneData->board?></td>
                                                                                <td><?php echo $oneData->cgpa."( Out of ".$oneData->out_of." )"?></td>
                                                                                <td><?php echo $oneData->passing_year?></td>
                                                                                <td><a href="profile.php?update_academic_id=<?php echo $oneData->id?>" style="width: 50%" class="btn btn-warning">Edit</a><a href="controller/deleteAcademic.php?id=<?php echo $oneData->id?>" class="btn btn-danger" style="width: 50%">Delete</a></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <h3 style="font-family: 'Agency FB';text-align: center">Add More</h3>
                                                                    <form action="controller/insertAcademic.php" method="post">
                                                                        <div class="panel-body" >
                                                                            <div class="row" >
                                                                                <div class="col-md-12" id="input_fields_wrap">
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Degree</label>
                                                                                            <input type="text" name="degree[]" class="form-control" placeholder="Degree" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Institute</label>
                                                                                            <input type="text" name="institute[]" class="form-control" placeholder="Institute" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Department/Group</label>
                                                                                            <input type="text" name="department[]" class="form-control" placeholder="Department/Group" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Board</label>
                                                                                            <input type="text" name="board[]" class="form-control" placeholder="Board"  />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">CGPA</label>
                                                                                            <input type="text" name="cgpa[]" class="form-control" placeholder="CGPA" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Out of</label>
                                                                                            <input type="text" name="out_of[]" class="form-control" placeholder="5 or 4" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Passign Year</label>
                                                                                            <input type="number" name="passing_year[]" class="form-control" placeholder="Passign Year" required />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            <div class="row">
                                                                                <div class="col-md-2">
                                                                                    <div class="form-group">
                                                                                        <button id="add_field_button" class="btn btn-default">Add Another Degree</button>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <div class="form-group">
                                                                                        <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                                        <input type="submit" class="btn btn-primary" value="Submit Information" required />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                    <?php
                                                                }
                                                                ?>


                                                        <?php
                                                            }
                                                        else{
                                                            ?>
                                                            <form action="controller/insertAcademic.php" method="post">
                                                                <div class="panel-body" >
                                                                    <div class="row" >
                                                                        <div class="col-md-12" id="input_fields_wrap">
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Degree</label>
                                                                                    <input type="text" name="degree[]" class="form-control" placeholder="Degree" required />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Institute</label>
                                                                                    <input type="text" name="institute[]" class="form-control" placeholder="Institute" required />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Department/Group</label>
                                                                                    <input type="text" name="department[]" class="form-control" placeholder="Department/Group" required />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Board</label>
                                                                                    <input type="text" name="board[]" class="form-control" placeholder="Board"  />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">CGPA</label>
                                                                                    <input type="text" name="cgpa[]" class="form-control" placeholder="CGPA" required />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Out of</label>
                                                                                    <input type="text" name="out_of[]" class="form-control" placeholder="5 or 4" required />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Passign Year</label>
                                                                                    <input type="number" name="passing_year[]" class="form-control" placeholder="Passign Year" required />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <button id="add_field_button" class="btn btn-default">Add Another Degree</button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                                <input type="submit" class="btn btn-primary" value="Submit Information" required />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        <?php
                                                        }
                                                        ?>


                                                    </div>
                                                </div>

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-th-list">
                            </span>Computer Literacy</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseFour" class="panel-collapse collapse">
                                                        <?php
                                                            if($exist_literacy){
                                                                ?>
                                                                <form action="controller/updateLiteracy.php" method="post">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="col-md-11">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Computer Literacy</label>
                                                                                    <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                                    <input type="text" class="form-control" name="literacy" value="<?php echo $oneLiteracy->literacy?>"/>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <input type="submit" class="btn btn-primary" value="Update Information" required />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <a href="controller/deleteLiteracy.php?id=<?php echo $oneLiteracy->id?>" class="btn btn-danger">Delete</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                        <?php
                                                            }
                                                        else{
                                                            ?>
                                                            <form action="controller/insertLiteracy.php" method="post">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-md-11">
                                                                            <div class="form-group">
                                                                                <label for="grantpo">Computer Literacy</label>
                                                                                <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                                <input type="text" class="form-control" name="literacy" placeholder="Computer Literacy" required />
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <input type="submit" class="btn btn-primary" value="Submit Information" required />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        <?php
                                                        }
                                                        ?>


                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-th-list">
                            </span>Language Communication</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseFive" class="panel-collapse collapse <?php if(isset($_GET['language_id']))echo "in";?>" >
                                                      <?php
                                                            if($existLanguage && empty($_GET['language_id'])){
                                                                ?>
                                                                    <div class="panel-body">

                                                                           <?php
                                                                                foreach ($allLanguage as $oneData){
                                                                                    ?>
                                                                        <div class="row">
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Language</label>
                                                                                            <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                                            <input type="text" class="form-control" name="language" value=<?php echo $oneData->language?> />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Performance</label>
                                                                                            <select class="form-control" name="performance">
                                                                                                <option value="Good" <?php if($oneData->performance=='Good'){echo "selected";}?>>Good</option>
                                                                                                <option value="Medium" <?php if($oneData->performance=='Medium'){echo "selected";}?>>Medium</option>
                                                                                                <option value="Weak" <?php if($oneData->performance=='Weak'){echo "selected";}?>>Weak</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Criteria</label>
                                                                                            <select class="form-control" name="criteria">
                                                                                                <option value="Speak" <?php if($oneData->critera=='Speak'){echo "selected";}?>>Speak</option>
                                                                                                <option value="Writing" <?php if($oneData->critera=='Writing'){echo "selected";}?>>Writing</option>
                                                                                                <option value="Speak & writing both" <?php if($oneData->critera=='Speak & writing both'){echo "selected";}?>>Speak & writing both</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Action</label><br>
                                                                                            <a href="profile.php?language_id=<?php echo $oneData->id?>" class="btn btn-primary">Edit</a>
                                                                                            <a href="controller/deleteLanguage.php?id=<?php echo $oneData->id?>" class="btn btn-danger">Delete</a>
                                                                                        </div>
                                                                                    </div>
                                                                        </div>
                                                                            <?php
                                                                                }
                                                                           ?>
                                                                        <hr>
                                                                        <form action="controller/insertLanguage.php" method="post">
                                                                            <div class="panel-body">
                                                                                <div class="row" id="input_fields_wrap4">
                                                                                    <div class="col-md-5">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Language</label>
                                                                                            <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                                            <input type="text" class="form-control" name="language[]" placeholder="Language" required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Performance</label>
                                                                                            <select class="form-control" name="performance[]">
                                                                                                <option value="Good">Good</option>
                                                                                                <option value="Medium">Medium</option>
                                                                                                <option value="Weak">Weak</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="grantpo">Criteria</label>
                                                                                            <select class="form-control" name="criteria[]">
                                                                                                <option value="Speak">Speak</option>
                                                                                                <option value="Writing">Writing</option>
                                                                                                <option value="Speak & writing both">Speak & writing both</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="panel-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <button id="add_field_button4" class="btn btn-default">Add Another Language</button>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <input type="submit" class="btn btn-primary" value="Submit Information" required />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </form>

                                                                    </div>

                                                        <?php
                                                            }
                                                            else if(isset($_GET['language_id'])){
                                                                ?>
                                                                <div class="panel-body">
                                                                    <form action="controller/updateLanguage.php" method="post">
                                                                        <div class="row">
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Language</label>
                                                                                    <input type="text" class="form-control" name="language" value=<?php echo $oneLanguage->language?> />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Performance</label>
                                                                                    <select class="form-control" name="performance">
                                                                                        <option value="Good" <?php if($oneLanguage->performance=='Good'){echo "selected";}?>>Good</option>
                                                                                        <option value="Medium" <?php if($oneLanguage->performance=='Medium'){echo "selected";}?>>Medium</option>
                                                                                        <option value="Weak" <?php if($oneLanguage->performance=='Weak'){echo "selected";}?>>Weak</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Criteria</label>
                                                                                    <select class="form-control" name="criteria">
                                                                                        <option value="Speak" <?php if($oneLanguage->critera=='Speak'){echo "selected";}?>>Speak</option>
                                                                                        <option value="Writing" <?php if($oneLanguage->critera=='Writing'){echo "selected";}?>>Writing</option>
                                                                                        <option value="Speak & writing both" <?php if($oneLanguage->critera=='Speak & writing both'){echo "selected";}?>>Speak & writing both</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                    <input type="hidden" name="id" value="<?php echo $oneLanguage->id?>">
                                                                                   <input type="submit" class="btn btn-primary" value="Update Information">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                                <?php
                                                            }
                                                      else{
                                                          ?>
                                                          <form action="controller/insertLanguage.php" method="post">
                                                              <div class="panel-body">
                                                                  <div class="row" id="input_fields_wrap4">
                                                                      <div class="col-md-5">
                                                                          <div class="form-group">
                                                                              <label for="grantpo">Language</label>
                                                                              <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                              <input type="text" class="form-control" name="language[]" placeholder="Language" required />
                                                                          </div>
                                                                      </div>
                                                                      <div class="col-md-3">
                                                                          <div class="form-group">
                                                                              <label for="grantpo">Performance</label>
                                                                              <select class="form-control" name="performance[]">
                                                                                  <option value="Good">Good</option>
                                                                                  <option value="Medium">Medium</option>
                                                                                  <option value="Weak">Weak</option>
                                                                              </select>
                                                                          </div>
                                                                      </div>
                                                                      <div class="col-md-3">
                                                                          <div class="form-group">
                                                                              <label for="grantpo">Criteria</label>
                                                                              <select class="form-control" name="criteria[]">
                                                                                  <option value="Speak">Speak</option>
                                                                                  <option value="Writing">Writing</option>
                                                                                  <option value="Speak & writing both">Speak & writing both</option>
                                                                              </select>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div class="panel-body">
                                                                  <div class="row">
                                                                      <div class="col-md-2">
                                                                          <div class="form-group">
                                                                              <button id="add_field_button4" class="btn btn-default">Add Another Language</button>
                                                                          </div>
                                                                      </div>
                                                                      <div class="col-md-2">
                                                                          <div class="form-group">
                                                                              <input type="submit" class="btn btn-primary" value="Submit Information" required />
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </form>
                                                        <?php
                                                      }
                                                      ?>

                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-th-list">
                            </span>Hobbies</a>
                                                        </h4>
                                                    </div>
                                                    <?php
                                                        if($exist_hobbie){
                                                            ?>
                                                            <form action="controller/updateHobby.php" method="post">
                                                                <div id="collapseSix" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <div class="row" >
                                                                            <div class="col-md-11">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Hobby</label>
                                                                                    <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                                    <input type="text" name="hobbies" class="form-control" value="<?php echo $oneHobbie->hobbies;?>" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <div class="row">

                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <input type="submit" class="btn btn-primary" value="Update Information" required />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <a href="controller/deleteHobby.php?id=<?php echo $oneHobbie->id?>" class="btn btn-danger">Delete</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </form>
                                                            <?php

                                                        }else{
                                                            ?>
                                                            <form action="controller/insertHobby.php" method="post">
                                                                <div id="collapseSix" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <div class="row" >
                                                                            <div class="col-md-11">
                                                                                <div class="form-group">
                                                                                    <label for="grantpo">Hobby</label>
                                                                                    <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                                                                    <input type="text" name="hobbies" class="form-control" placeholder="Hobby" required />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <div class="row">

                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <input type="submit" class="btn btn-primary" value="Submit Information" required />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </form>
                                                    <?php
                                                        }

                                                    ?>

                                                </div>
                                                <?php
                                                   if($exist_field){
                                                       $serial=1;
                                                       foreach ($allNewField as $oneData){
                                                           ?>
                                                           <div class="panel panel-default">
                                                               <div class="panel-heading">
                                                                   <h4 class="panel-title">
                                                                       <a data-toggle="collapse" data-parent="#accordion" href="<?php echo "#".$serial?>"><span class="glyphicon glyphicon-th-list">
                            </span><?php echo $oneData->title?></a>
                                                                   </h4>
                                                               </div>
                                                               <?php
                                                               if($exist_field){
                                                                   ?>
                                                                   <form action="controller/updateTable.php" method="post">
                                                                       <div id="<?php echo $serial?>" class="panel-collapse collapse">
                                                                           <div class="panel-body">
                                                                               <div class="row" >
                                                                                   <div class="col-md-11">
                                                                                       <div class="form-group">
                                                                                           <input type="hidden" name="id" value="<?php echo $oneData->id?>">
                                                                                           <input type="text" class="form-control" name="field" value="<?php echo $oneData->title?>">
                                                                                           <br><br>
                                                                                           <textarea id="<?php echo "text".$serial;?>" class="form-control" rows="16" name="content"> <?php echo $oneData->content?></textarea>

                                                                                       </div>
                                                                                   </div>
                                                                               </div>
                                                                           </div>
                                                                           <div class="panel-body">
                                                                               <div class="row">
                                                                                   <div class="col-md-3">
                                                                                       <div class="form-group">
                                                                                           <input type="submit" value="Update" class="btn btn-primary">
                                                                                           <a href="controller/deleteTable.php?id=<?php echo $oneData->id?>" class="btn btn-danger">Delete</a>
                                                                                       </div>
                                                                                   </div>

                                                                               </div>
                                                                           </div>
                                                                       </div>
                                                                   </form>
                                                                       
                                                                   <?php

                                                               }
                                                               ?>

                                                           </div>
                                                <?php
                                                           $serial++;
                                                       }
                                                   }
                                                ?>

                                            </div>
                                        </div>
                                <div class="col-md-12">
                                    <form action="controller/addNewField.php" method="post">
                                        <label>Add Another Category</label>
                                        <input type="text" name="field" list="productName" class="form-control" placeholder="Field Name"/>
                                        <div class="form-group">
                                            <label>Content</label>
                                            <textarea id="text" class="form-control" rows="16" name="content" placeholder="Content" required></textarea>
                                        </div><!--//form-group-->
                                        <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>">
                                        <input type="submit">
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>

<script type="text/javascript">
    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 300,
            height: 300,
            type: 'rectangle'
        },
        boundary: {
            width: 320,
            height: 320
        }
    });

    $('#upload').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });

        }
        reader.readAsDataURL(this.files[0]);
    });

    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {

            $.ajax({
                url: "ajaxpro.php",
                type: "POST",
                data: {"image":resp},
                dataType: 'json',
                success: function (data) {
                    html = '<img src="' + resp + '" class="img-responsive" width="100%"/>';
                    html2 = '<input type="hidden" name="pic" value="' + data + '" required/>';
                    $("#upload-demo-i").html(html);
                    $("#upload-demo-i2").html(html2);

                }
            });
        });
    });

</script>
<?php include("templateLayout/script/templateScript.php");?>
<?php include("templateLayout/script/addAcademic.php");?>
<?php include("templateLayout/script/addAchievement.php");?>
<?php include("templateLayout/script/addLiteracy.php");?>
<?php include("templateLayout/script/addLanguage.php");?>
<?php include("templateLayout/script/addHobbie.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>


<script> CKEDITOR.replace( 'text' );</script>

<?php
    $serial=1;
    foreach ($allNewField as $OneData){
        ?>
        <script> CKEDITOR.replace( '<?php echo "text".$serial?>' );</script>
      <?php
        $serial++;
    }
?>
<script> CKEDITOR.replace( 'text2' );</script>
<script>
    baguetteBox.run('.tz-gallery');
</script>

</body>
</html>


